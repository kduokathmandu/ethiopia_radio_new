/**
 * NotificationHelper.java
 * Implements the NotificationHelper class
 * A NotificationHelper creates and configures a notification
 * <p>
 * This file is part of
 * TRANSISTOR - Radio App for Android
 * <p>
 * Copyright (c) 2015-16 - Y20K.org
 * Licensed under the MIT-License
 * http://opensource.org/licenses/MIT
 */


package com.ethiopiaradios.kduo.helpers;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Build;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;
import android.support.v4.media.session.MediaSessionCompat;

import com.ethiopiaradios.kduo.MainActivity;
import com.ethiopiaradios.kduo.PlayerService;
import com.ethiopiaradios.kduo.R;
import com.ethiopiaradios.kduo.core.Station;

import static android.content.Context.NOTIFICATION_SERVICE;


/**
 * NotificationHelper class
 */
public final class NotificationHelper {

    /* Define log tag */
    private static final String LOG_TAG = NotificationHelper.class.getSimpleName();


    /* Main class variables */
    private static Notification mNotification;
    private static Service mService;
//    private static MediaSessionCompat mSession;
    private static String mStationMetadata;
    private static final String PRIMARY_CHANNEL = "PRIMARY_CHANNEL_ID";
    private static final String PRIMARY_CHANNEL_NAME = "PRIMARY";


    /* Create and put up notification */
    public static void show(final Service service, MediaSessionCompat session, Station station, int stationID, String stationMetadata) {
        // save service and session
        mService = service;
//        mSession = session;
        mStationMetadata = stationMetadata;

        // build notification
        station.setPlaybackState(true);

        // display notification

        mNotification = getNotificationBuilder(station, stationID, mStationMetadata).build(); // TODO: change -> Station object contains metadata, too

        service.startForeground(TransistorKeys.PLAYER_SERVICE_NOTIFICATION_ID, mNotification);
    }



    /* Updates the notification */
    public static void update(Station station, int stationID, String stationMetadata, MediaSessionCompat session) {

        // session can be null on update
        if (session != null) {
//            mSession = session;
        }

        // metadata can be null on update
        if (stationMetadata != null) {
            mStationMetadata = stationMetadata;
        }

        // build notification
        mNotification = getNotificationBuilder(station, stationID, mStationMetadata).build();

        // display updated notification
        NotificationManager notificationManager = (NotificationManager) mService.getSystemService(NOTIFICATION_SERVICE);
        notificationManager.notify(TransistorKeys.PLAYER_SERVICE_NOTIFICATION_ID, mNotification);

        if (!station.getPlaybackState()) {
            // make notification swipe-able
            mService.stopForeground(false);
        }

    }


    /* Stop displaying notification */
    public static void stop() {
        if (mService != null) {
            mService.stopForeground(true);
        }
    }


    /* Creates a notification builder */
    private static NotificationCompat.Builder getNotificationBuilder(Station station, int stationID, String stationMetadata) {

        // explicit intent for notification tap
        Intent tapActionIntent = new Intent(mService, MainActivity.class);
        tapActionIntent.setAction(TransistorKeys.ACTION_SHOW_PLAYER);
        tapActionIntent.putExtra(TransistorKeys.EXTRA_STATION, station);
        tapActionIntent.putExtra(TransistorKeys.EXTRA_STATION_ID, stationID);


        // explicit intent for stopping playback
        Intent stopActionIntent = new Intent(mService, PlayerService.class);
        stopActionIntent.setAction(TransistorKeys.ACTION_STOP);

        // explicit intent for starting playback
        Intent playActionIntent = new Intent(mService, PlayerService.class);
        playActionIntent.setAction(TransistorKeys.ACTION_PLAY);

        // explicit intent for swiping notification
        Intent swipeActionIntent = new Intent(mService, PlayerService.class);
        swipeActionIntent.setAction(TransistorKeys.ACTION_DISMISS);

        // artificial back stack for started Activity.
        // -> navigating backward from the Activity leads to Home screen.
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(mService);
//        // backstack: adds back stack for Intent (but not the Intent itself)
//        stackBuilder.addParentStack(MainActivity.class);
        // backstack: add explicit intent for notification tap
        stackBuilder.addNextIntent(tapActionIntent);

        // pending intent wrapper for notification tap
        PendingIntent tapActionPendingIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);
//        PendingIntent tapActionPendingIntent = PendingIntent.getService(mService, 0, tapActionIntent, 0);
        // pending intent wrapper for notification stop action
        PendingIntent stopActionPendingIntent = PendingIntent.getService(mService, 10, stopActionIntent, 0);
        // pending intent wrapper for notification start action
        PendingIntent playActionPendingIntent = PendingIntent.getService(mService, 11, playActionIntent, 0);
        // pending intent wrapper for notification swipe action
        PendingIntent swipeActionPendingIntent = PendingIntent.getService(mService, 12, swipeActionIntent, 0);

        // create media style

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationManager manager = (NotificationManager) mService.getSystemService(Context.NOTIFICATION_SERVICE);
            NotificationChannel channel = new NotificationChannel(PRIMARY_CHANNEL, PRIMARY_CHANNEL_NAME, NotificationManager.IMPORTANCE_LOW);
            channel.setLockscreenVisibility(Notification.VISIBILITY_PRIVATE);
            manager.createNotificationChannel(channel);
        }

//        String channel;
//
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N)
//            channel = createChannel();
//        else {
//            channel = "";
//        }
        // construct notification in builder
        NotificationCompat.Builder builder;
        builder = new NotificationCompat.Builder(mService,PRIMARY_CHANNEL);
        builder.setContentText(mService.getString(R.string.app_name));
        builder.setVisibility(NotificationCompat.VISIBILITY_PUBLIC);
        builder.setSmallIcon(R.drawable.ic_notification_small_24dp);
        builder.setLargeIcon(getStationIcon(mService, station));
        builder.setContentTitle(station.getStationName());
        builder.setChannelId(PRIMARY_CHANNEL);
        builder.setContentText(stationMetadata);
        builder.setShowWhen(false);
        builder.setStyle(new android.support.v4.media.app.NotificationCompat.MediaStyle().setShowCancelButton(true).setCancelButtonIntent(swipeActionPendingIntent).setShowActionsInCompactView(0));
        builder.setContentIntent(tapActionPendingIntent);
        builder.setDeleteIntent(swipeActionPendingIntent);



        if (station.getPlaybackState()) {
            builder.addAction(R.drawable.ic_stop_white_36dp, mService.getString(R.string.notification_stop), stopActionPendingIntent);
        } else {
            builder.addAction(R.drawable.ic_play_arrow_white_36dp, mService.getString(R.string.notification_play), playActionPendingIntent);
        }

        return builder;
    }



    /* Get station image for notification's large icon */
    private static Bitmap getStationIcon(Context context, Station station) {
        if (station == null) {
            return null;
        }
        // create station image icon
        Bitmap stationIcon;
        ImageHelper imageHelper;
        Bitmap stationImage;



        stationImage = null;


        imageHelper = new ImageHelper(stationImage, context);
        stationIcon = imageHelper.createStationIcon(512);


        return stationIcon;
    }

}
